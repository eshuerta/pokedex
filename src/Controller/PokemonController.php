<?php
// src/Controller/PokemonController.php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

define("API_URL","https://pokeapi.co/api/v2/");
class PokemonController extends AbstractController
{    
    private $session;
    
    public function __construct(SessionInterface $session) {
        $this->session = $session;
    }

    /**
    * @Route("/pokemon/")
    */
    public function getPokemons() {
       
        $client = HttpClient::create();
        
        // Si ya los obtuve no los vuelvo a buscar
        if ($this->session->get('pokemonList') && is_array($this->session->get('pokemonList'))) {
            $pokeList = $this->session->get('pokemonList');
            // selecciono 20 pokemones al azar para motrar algo en la pantalla inicial
            shuffle($pokeList);
            return $this->render('pokemones.html.twig', [
                'pokemones' => array_slice($pokeList, 0, 20),
                'error' => false,
            ]);
        } 
        
        // Todos los poke
        $response = $client->request('GET', API_URL . '/pokedex/1');

        if ($response->getStatusCode() === 200) {
            $content = $response->getContent();
            $content = $response->toArray();
            
            $pokeList = $content["pokemon_entries"];
            // Guardo la lista total
            $this->session->set('pokemonList', $pokeList);
            
            // selecciono 20 pokemones al azar para motrar algo en la pantalla inicial
            shuffle($pokeList);
            return $this->render('pokemones.html.twig', [
                'pokemones' => array_slice($pokeList, 0, 20),
                'error' => false,
            ]);
        } else {
            return $this->render('pokemones.html.twig', [
                'pokemones' => null,
                'error' => true,
            ]);
        }
    }

    /**
    * @Route("/pokemon/search", methods={"POST"})
    */
    public function filterPokemonByNameOrId() {
        // La busqueda
        $request = Request::createFromGlobals();
        $searchValue = $request->request->get('searchValue');
        // lista de todos los poke
        $pokemonList = $this->session->get('pokemonList');

        if ($searchValue && trim($searchValue) !== '' ) {
            // Busco por id?
            $searchForId = is_numeric($searchValue);
            
            // Si busco por nombre
            if (!$searchForId) {
                $pokemonFiltered = array_filter($pokemonList, function ($var) use  ($searchValue) {
                    return (strpos($var["pokemon_species"]["name"], $searchValue) !== false);
                });
            } else { // Busco por ID
                $pokemonFiltered = array_filter($pokemonList, function ($var) use  ($searchValue) {
                    return ($var["entry_number"] == $searchValue);
                });
            }        

            return $this->render('pokemones.html.twig', [
                'pokemones' => $pokemonFiltered,
                'error' => false,
                'search' => $searchValue,
                'count' => count($pokemonFiltered),
                'byId' => $searchForId
            ]);
        
        } else {
            return $this->render('pokemones.html.twig', [
                'pokemones' => $pokemonList,
                'error' => false,
            ]);
        }
    }

    /**
    * @Route("/pokemon/info")
    */
    public function getPokemonData() {
       
        $client = HttpClient::create();
        $request = Request::createFromGlobals();
        $pokemonId = $request->request->get('pokemonId');

        // Pokemon info
        $response = $client->request('GET', API_URL . '/pokemon/' . $pokemonId);

        if ($response->getStatusCode() === 200) {
            $content = $response->getContent();
            $content = $response->toArray();            
            return $this->json($content);          
        } else {          
            return $this->json(false);   
        }        
    }
   
}