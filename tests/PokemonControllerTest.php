<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage;

define("API_URL","https://pokeapi.co/api/v2/");
class PokemonControllerTest extends WebTestCase
{    
    public function getPokemonsTest() {
        $client = static::createClient();
        
        $response = $client->request('GET', API_URL . '/pokedex/1');
        
        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getContent();
        $content = $response->toArray();     
        
        $this->assertTrue(is_array($content), "Respuesta es array");
    }

    public function getPokemonsDataTest() {    
    }

    public function filterPokemonByNameOrIdTest() {        
    }
}